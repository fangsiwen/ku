# python环境搭建
(一）python下载

打开python官网：

Welcome to Python.org

找到自己需要的版本下载安装包。

![image](images/_1k4L__miRB1Z2ecj6tBhWUzTN3JqhApjXUvZhwJ3y8.png)



(二）python安装

1.打开下载好的安装包，选择自定义安装（Cutormize installation)

勾选Install for all users选项。

2.自定义安装路径后即可安装。

（三）检查python是否正常安装成功

打开cmd,在终端输入python。出现版本信息（类似下图）即为安装成功。

![image](images/mKo8DCqj5tlFwMQYROzAtzXeuizsWZMAmq-y_mVj_44.png)

再输入print(“hello”),如下图即为安装成功。

![image](images/s2sP93owiy-9esNqjKPcseipbptliHQvhdHjQZyC2o0.png)



（四)配置python环境变量

在”设置“中找到“关于”，点击”高级系统设置“，点击”环境变量“。

在”用户变量“中找到”Path变量“并双击打开。

找到安装python环境的目录，分别点击”新建“并把”python目录“以及python目录下的”scripts目录“添加到里面，如下图。

![image](images/IKwQu8C5yOyp7Xm5u1Z9Ls-Vn6WfdOU-XETSP22JlWY.png)

![image](images/3OUhf1zHfe5Cei8MWfeUbrpkzrxTMSivqFC39rxhaq0.png)



![image](images/RJM8av8qdsDmIrfVDMQQyj6O9QkivgqvYXD4b0JjP8s.png)



















